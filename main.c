#include <ncurses.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>

#include "lang/spanish.h"

int stdin_default_flags, screen_height, screen_width, loop_delay = 10000, ball_factor = 5, ball_count = 0, points = 0;
struct platform_t { int phor, width; char *string; } platform;
struct ball_t { int pver, phor, goright, goup; } ball;

void centeredPrint(char *string) { mvprintw(screen_height >> 1, ( screen_width - strlen(string) ) >> 1, string); }
void printPlataform() { mvprintw(screen_height - 1, platform.phor, platform.string); }
void printBall() { mvprintw(ball.pver, ball.phor, "O"); }

int movePlataform()
{
	char tecla = getch();

	if(tecla == 4 && platform.phor > 0) { platform.phor--; }
	if(tecla == 5 && platform.phor + platform.width < screen_width) { platform.phor++; }
	if(tecla == 113) { return 1; } return 0;
}

int moveBall()
{
	ball_count++; ball_count %= ball_factor; if(ball_count) { return 0; }

	if(ball.goright) { ball.phor++; } else { ball.phor--; }
	if(ball.goup) { ball.pver--; } else { ball.pver++; }

	if(ball.pver == screen_height - 1)
	{
		if((ball.phor >= platform.phor) && (ball.phor <= (platform.phor + platform.width - 1))) points++;
		else return 1;
	}

	if(ball.pver == 0 || ball.pver == screen_height - 1) { ball.goup = !ball.goup; }
	if(ball.phor == 0 || ball.phor == screen_width - 1) { ball.goright = !ball.goright; }

	return 0;
}

int main()
{
	initscr();
	noecho();
	curs_set(0);
	keypad(stdscr,true);
	srand(time(NULL));

	stdin_default_flags = fcntl(0,F_GETFL);
	screen_height = getmaxy(stdscr);
	screen_width = getmaxx(stdscr);

	platform.string = "*********";
	platform.width = strlen(platform.string);
	platform.phor = rand() % ( 1 + screen_width - platform.width );

	ball.pver = screen_height - 2;
	ball.phor = platform.phor + ( platform.width >> 1 );
	ball.goright = rand() % 2;
	ball.goup = 1;

	if(screen_height < 24 || screen_width < 80)
	{
		endwin();
		printf(S_TTYSIZE);
		return 1;
	}

	centeredPrint(S_WELCOME);
	refresh();
	getch();

	fcntl(0,F_SETFL,stdin_default_flags | O_NONBLOCK);

	while(1)
	{
		erase();
		printBall();
		printPlataform();
		refresh();

		if(moveBall() || movePlataform()) { break; }
		usleep(loop_delay);
	}

	fcntl(0,F_SETFL,stdin_default_flags);

	erase();
	char goodbye_string[75], trash_buffer[75];
	snprintf(goodbye_string, 70, S_GOODBYE, points);
	centeredPrint(goodbye_string); refresh();
	scanw("%70s", trash_buffer);

	endwin();
	return 0;
}
