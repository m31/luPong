### ¿ Qué es luPong ?

luPong es un juego por consola de comandos similar al viejo arkanoid, en el
cual hay una bola que va rebotando y tienes que impedir que caiga al suelo.

Es posible que en un futuro agregue soporte para color y multijugador en red,
aunque con lo vago que soy programando, no conteis con ello ...

### Modificar el idioma del juego

Para cambiar los strings de idioma de juego, edita en main.c la siguiente línea:

#include "lang/spanish.h"

Y cámbiala por esta otra:

#include "lang/klingon.h"

### Parámetros del juego

ball_factor: para una pelota lenta, especificar un valor elevado, y viceversa. Este
factor en realidad representa la cantidad de ticks del juego ( cada cuanto tiempo se
lee el teclado y se procesa ) que corresponden a un procesamiento del movimiento de
la bola. Si pones un valor bajo, y por tanto una pelota rápida, preparate porque te
vas a dejar los dedos jugando ...

platform_string: Es la cadena de asteriscos que representan la pala que golpea la bola
obviamente, cuanto mas largo sea este string, mas facil darle a la bola ...
